const { DataTypes, Model } = require('sequelize')
const sequelize = require('./sequelize.js')

class Barang extends Model {}

Barang.init({
    Nama: {
        type: DataTypes.STRING
    },
    Harga: {
        type: DataTypes.DOUBLE
    },
    Stok: {
        type: DataTypes.DOUBLE
    }
},{
    sequelize,
    modelName: 'barang'
})

module.exports = Barang