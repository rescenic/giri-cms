const express = require('express')
const app = express()
const port = 9500
const Barang = require('./barang.js')
const helpers = require('./middlewares.js')
const UserController = require('./user-controller.js')
const CorpController = require('./corp-controller.js')
const BarangController = require('./barang-controller.js')
const TransController = require('./trans-controller.js')

app.use(express.json())
app.use(express.urlencoded({ limit: '50mb', extended: true }))

app.post('/users/register',UserController.register)
app.post('/users/login',UserController.login)

app.post('/corp/add',helpers.header,CorpController.add)
app.get('/corp/data',helpers.header,CorpController.data)

app.post('/items/add',helpers.header,BarangController.add)
app.get('/items/data',helpers.header,BarangController.data)

app.post('/trans/add',helpers.header,TransController.add)
app.get('/trans/data',helpers.header,TransController.data)
app.get('/trans/export',TransController.eksport)


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})