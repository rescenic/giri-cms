const { DataTypes, Model } = require('sequelize')
const sequelize = require('./sequelize.js')

class Corp extends Model {}

Corp.init({
    Kode: {
        type: DataTypes.STRING
    },
    Nama: {
        type: DataTypes.STRING
    } 
},{
    sequelize,
    modelName: 'corp'
})

module.exports = Corp