-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2022 at 04:28 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `giri`
--

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

DROP TABLE IF EXISTS `barangs`;
CREATE TABLE IF NOT EXISTS `barangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nama` text DEFAULT NULL,
  `Harga` double DEFAULT NULL,
  `Stok` double DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `barangs`
--

TRUNCATE TABLE `barangs`;
--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `Nama`, `Harga`, `Stok`, `createdAt`, `updatedAt`) VALUES
(1, 'bolpen', 5000, 32, '2022-07-12 08:32:03', '2022-07-12 08:32:03'),
(2, 'pensil', 3000, 31, '2022-07-12 13:45:28', '2022-07-12 13:45:28');

-- --------------------------------------------------------

--
-- Table structure for table `corps`
--

DROP TABLE IF EXISTS `corps`;
CREATE TABLE IF NOT EXISTS `corps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Kode` text DEFAULT NULL,
  `Nama` text DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `corps`
--

TRUNCATE TABLE `corps`;
--
-- Dumping data for table `corps`
--

INSERT INTO `corps` (`id`, `Kode`, `Nama`, `createdAt`, `updatedAt`) VALUES
(1, 'ABC123', 'PT Hibiscus Berseri', '2022-07-12 13:23:39', '2022-07-12 13:23:39'),
(2, 'ZAT676', 'PT Wanita Ceria', '2022-07-12 13:31:58', '2022-07-12 13:31:58');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TglInput` date DEFAULT NULL,
  `Corp` text DEFAULT NULL,
  `Item` text DEFAULT NULL,
  `Qty` double DEFAULT NULL,
  `Harga` double DEFAULT NULL,
  `Total` double DEFAULT NULL,
  `Sisa` double DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `transactions`
--

TRUNCATE TABLE `transactions`;
--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `TglInput`, `Corp`, `Item`, `Qty`, `Harga`, `Total`, `Sisa`, `createdAt`, `updatedAt`) VALUES
(1, '1990-01-01', 'PT Hibiscus Berseri', 'pensil', 2, 3000, 6000, 29, '2022-07-12 14:12:43', '2022-07-12 14:12:43'),
(2, '1990-01-02', 'PT Wanita Ceria', 'bolpen', 5, 5000, 25000, 27, '2022-07-12 14:13:13', '2022-07-12 14:13:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` text DEFAULT NULL,
  `Username` text DEFAULT NULL,
  `Password` text DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `Name`, `Username`, `Password`, `createdAt`, `updatedAt`) VALUES
(1, 'HibiscusHT', 'tes@email.com', '8f4047e3233b39e4444e1aef240e80aa', '2022-07-12 12:36:14', '2022-07-12 12:36:14');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
